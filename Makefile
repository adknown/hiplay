#
# Makefile for the hiPlay project
#
OS=darwin
ARCH=amd64

GBDEP=github.com/constabulary/gb/...

GITHASH=$(shell git rev-parse --short HEAD)
CREATED=$(shell date "+%Y-%m-%dT%H:%M:%SZ")

# LDFLAGS  = -s # strip all debug info
LDFLAGS += -X parser.Githash=$(GITHASH)
LDFLAGS += -X parser.Created=$(CREATED)

all: build

init:
	go get -u $(GBDEP)

deps: init
	gb vendor update --all

purge: init
	gb vendor purge

build:
	GOOS=$(OS) GOARCH=$(ARCH) CGO_ENABLED=0 \
		gb build -ldflags "$(LDFLAGS)" all

clean:
	rm  -f bin/*
	rm -rf pkg/*

.PHONY: all init deps purge build clean
