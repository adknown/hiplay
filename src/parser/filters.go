package parser

import (
	"strings"
)

const (
	emoticonMaxLen = 15
)

type Filterer interface {
	Filter(string) (string, bool)
}

type Mention struct{}

func (this *Mention) Filter(word string) (string, bool) {
	if strings.HasPrefix(word, "@") {
		return word[1:], true
	}

	return "", false
}

type Emoticon struct{}

func (this *Emoticon) Filter(word string) (string, bool) {
	var emotion string

	if strings.HasPrefix(word, "(") {
		if strings.HasSuffix(word, ")") {
			emotion = word[1 : len(word)-1]
		} else {
			emotion = word[1 : emoticonMaxLen+1]
		}

		//
		// just for funny the emotions are real values
		// obtained from https://www.hipchat.com/emoticons
		// so, i'll try to validate our real life... more/less :)
		//
		if NewEmotion(emotion).Validate() {
			return emotion, true
		}
	}

	return "", false
}

type Url struct{}

func (this *Url) Filter(word string) (string, bool) {
	if strings.HasPrefix(word, "http://") || strings.HasPrefix(word, "https://") {
		return word, true
	}

	return "", false
}
