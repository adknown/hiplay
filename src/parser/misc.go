package parser

//
// Auxillary functions
//
func index(vs []string, t string) int {
	for i, v := range vs {
		if v == t {
			return i
		}
	}

	return -1
}

func include(vs []string, t string) bool {
	return index(vs, t) >= 0
}
