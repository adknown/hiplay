package parser

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"golang.org/x/net/html"
)

//
// parser results
//
type Link struct {
	Url   string `json:"url"`
	Title string `json:"title"`
}

type Result struct {
	indented bool `json:"-"`

	Mentions  []string `json:"mentions,omitempty"`
	Emoticons []string `json:"emoticons,omitempty"`
	Links     []*Link  `json:"links,omitempty"`
}

func (this *Result) String() string {
	var buf []byte

	switch this.indented {
	case true:
		buf, _ = json.MarshalIndent(this, "", "  ")
	case false:
		buf, _ = json.Marshal(this)
	}

	return string(buf)
}

//
// parser config
//
type Config struct {
	Indented         bool
	ParseTitle       bool
	ConnectTimeout   time.Duration
	ReadWriteTimeout time.Duration
}

//
// parser implementation
//
type Parser struct {
	config *Config
	result *Result

	wg sync.WaitGroup
}

func New(conf *Config) *Parser {
	return &Parser{
		config: conf,
		result: &Result{indented: conf.Indented},
	}
}

func (this *Parser) Run(input string) (*Result, error) {
	log.Printf("parser processing [%s]...", input)

	done := make(chan struct{})

	words := strings.Split(input, " ")

	mch := NewCollector().Collect(words, &Mention{}, &this.wg)
	ech := NewCollector().Collect(words, &Emoticon{}, &this.wg)
	uch := NewCollector().Collect(words, &Url{}, &this.wg)

	go func() {
		for {
			select {
			case <-done:
				break
			case mention := <-mch:
				this.result.Mentions = append(this.result.Mentions, mention)
			case emoticon := <-ech:
				this.result.Emoticons = append(this.result.Emoticons, emoticon)
			case url := <-uch:
				this.result.Links = append(this.result.Links, &Link{Url: url})
			}
		}
	}()

	this.wg.Wait()
	done <- struct{}{}

	//
	// if it neccessary to retrieve res.body.title so slowly?
	//
	if this.config.ParseTitle && this.result.Links != nil {
		this.parseTitles()
	}

	return this.result, nil
}

func (this *Parser) parseTitles() {
	for idx, link := range this.result.Links {
		this.wg.Add(1)

		//
		// number of goroutines = len(result.Links)
		//
		go func(idx int, link *Link) {
			defer this.wg.Done()

			log.Printf("parser processing link[%d]: %s", idx, link.Url)
			link.Title = this.parseTitle(link.Url)
		}(idx, link)
	}

	this.wg.Wait()
}

func (this *Parser) parseTitle(url string) string {
	//
	// specific http client with timeout 1s
	// alternatively we could change this approach to context.WithTimeout
	//
	client := http.Client{
		Transport: &http.Transport{
			Dial: timeoutDialer(
				this.config.ConnectTimeout, this.config.ReadWriteTimeout,
			),
		},
	}

	res, err := client.Get(url)
	if err != nil {
		log.Printf("parser http get error [%s]", err)
		return "n/a"
	}

	defer func() {
		res.Body.Close()
	}()

	//
	// parsing response body
	//
	depth := 0
	tokenizer := html.NewTokenizer(res.Body)

	for {
		token := tokenizer.Next()

		switch token {
		case html.ErrorToken:
			break
		case html.TextToken:
			if depth > 0 {
				return string(tokenizer.Text())
			}
			break
		case html.StartTagToken, html.EndTagToken:
			tn, _ := tokenizer.TagName()

			if len(tn) == 5 && string(tn) == "title" {
				if token == html.StartTagToken {
					depth++
				} else {
					depth--
				}
			}
		}
	}
}

func timeoutDialer(timeout time.Duration, rwTimeout time.Duration) func(net, addr string) (c net.Conn, err error) {
	return func(netw, addr string) (net.Conn, error) {
		conn, err := net.DialTimeout(netw, addr, timeout)
		if err != nil {
			return nil, err
		}
		conn.SetDeadline(time.Now().Add(rwTimeout))
		return conn, nil
	}
}
