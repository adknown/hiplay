package parser

import (
	"sync"
)

type Collector struct{}

func NewCollector() *Collector {
	return &Collector{}
}

func (this *Collector) Collect(words []string, filter Filterer, wg *sync.WaitGroup) chan string {
	wg.Add(1)

	ch := make(chan string)

	go func() {
		defer wg.Done()

		for _, word := range words {
			if w, ok := filter.Filter(word); ok {
				ch <- w
			}
		}

		ch = nil
	}()

	return ch
}
