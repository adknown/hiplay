package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"parser"
)

var (
	optParseTitle       = flag.Bool("parse-title", false, "Parsing url title or not (slower if true)")
	optIndented         = flag.Bool("indented", true, "Display parser results in human readable view")
	optConnectTimeout   = flag.Duration("connect-timeout", 1000000000, "Http max connect timeout (Xms, Xs)")
	optReadWriteTimeout = flag.Duration("readwrite-timeout", 1000000000, "Http max read/write timeout (Xms, Xs)")
	optInput            = flag.String("input", "", "Input string in HiPlay format [@x, (x), http://x]")

	errNone = errors.New("done")
)

type App struct {
	parser *parser.Parser
}

func NewApp(indented, parsetitle bool, connectTimeout, readWriteTimeout time.Duration) *App {
	return &App{
		parser: parser.New(
			&parser.Config{
				Indented:         indented,
				ParseTitle:       parsetitle,
				ConnectTimeout:   connectTimeout,
				ReadWriteTimeout: readWriteTimeout,
			},
		),
	}
}

func (this *App) Run(input string) error {
	var (
		err error
		res *parser.Result
	)

	if res, err = this.parser.Run(input); err != nil {
		return err
	}

	fmt.Printf("%s", res)
	return errNone
}

func main() {
	flag.Parse()

	if *optInput == "" {
		log.Fatalf("nothing to parse. see --input parameter!")
	}

	errs := make(chan error, 2)

	go func() {
		log.Printf(
			"hiPlay parser: %s-%s-%s (%s)",
			parser.Version, parser.Staging, parser.Githash, parser.Created,
		)

		errs <- NewApp(
			*optIndented, *optParseTitle, *optConnectTimeout, *optReadWriteTimeout,
		).Run(*optInput)
	}()

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
		errs <- fmt.Errorf("%s", <-c)
	}()

	log.Printf("parser terminated [%s]", <-errs)
}
