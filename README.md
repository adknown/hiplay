HipChat parser (hiPlay)
======================

## Intended for?

Just for playing with different golang technologies such as reflection, channels, interfaces
and other nice things... Playing with http timeouts and source code decomposition for build test cases more easily...

>NB: This version of parser contains emoticons validator (obtained from https://www.hipchat.com/emoticons) which filter unknown emoticons. So, use (emoticon) value according to url link above...

### Whats inside?

- Cmd - command line utility parser (try cmd -h)
- Parser - parser library toy which implements specific rules
- Service - RESTFul http service for processing input hipchat requests (just for funny?)

### How to clone the project

You've to execute command below inside your own local projects folder

	$ git clone git@bitbucket.org:adknown/hiplay.git

### How to build?

Check the Makefile $(OS), $(ARCH) variables and change them depending on your platform/architecture
as described on https://golang.org/doc/install/source link in "Optional environment variables" chapter

	- OS=linux; ARCH=386
	- OS=darwin; ARCH=amd64
	- OS=windows; ARCH=amd64
	- ... so on ...

And execute followed commands inside the project folder (Makefile location)

	$ make deps
	$ make

### How to play?

Just execute "./bin/cmd -h" command from command line

Parser command line utility help

	$ ./bin/cmd -h

	Usage of ./bin/cmd:
	  -indented
	    	Display parser results in human readable view (default true)
	  -input string
	    	Input string in HiPlay format [@x, (x), http://x]
	  -parse-title
	    	Parsing url title or not (slower if true)
	  -connect-timeout duration
	    	Http max connect timeout (Xms, Xs) (default 1s)
	  -readwrite-timeout duration
	    	Http max read/write timeout (Xms, Xs) (default 1s)

#### Parse several hipchat input string

Parse simple input string:

	./bin/cmd --parse-title=false --input="Hi, @bob! (cofee) (kiss)"

Result:

```json
	{
	  "mentions": [
	    "bob"
	  ],
	  "emoticons": [
	    "cofee",
	    "kiss"
	  ]
	}
```
Parse input string with remote link title:

	$ ./bin/cmd --parse-title=true \
		--input="@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"

Result:

```json
	{
	  "mentions": [
	    "bob",
	    "john"
	  ],
	  "emoticons": [
	    "success"
	  ],
	  "links": [
	    {
	      "url": "https://twitter.com/jdorfman/status/430511497475670016",
	      "title": "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""
	    }
	  ]
	}
```

And the hardcore at the end:

Below the very and very busy sites. So, the request timeout with customised http client is cancelled after 1-2secs:

	$ ./bin/cmd  --parse-title=true \
		--input="@vlad @irina @alex (smile) (beer) like the unbreakable news; http://ukr.net http://unian.net http://korrespondent.net http://utro.ua"

Result:

	- 2016/10/01 15:37:44 parser http get error \
		[Get http://ukr.net: net/http: request canceled while waiting for connection \
			(Client.Timeout exceeded while awaiting headers)]

	- 2016/10/01 15:37:45 parser http get error \
		[Get http://unian.net: net/http: request canceled while waiting for connection \
			(Client.Timeout exceeded while awaiting headers)]

	- 2016/10/01 15:37:46 parser http get error \
		[Get http://korrespondent.net: net/http: request canceled while waiting for connection \
			(Client.Timeout exceeded while awaiting headers)]

	- 2016/10/01 15:37:47 parser http get error \
		[Get http://utro.ua: net/http: request canceled while waiting for connection \
			(Client.Timeout exceeded while awaiting headers)]

```json
	{
	  "mentions": [
	    "vlad",
	    "irina",
	    "alex"
	  ],
	  "emoticons": [
	    "smile",
	    "beer"
	  ],
	  "links": [
	    {
	      "url": "http://ukr.net",
	      "title": "n/a"
	    },
	    {
	      "url": "http://unian.net",
	      "title": "n/a"
	    },
	    {
	      "url": "http://korrespondent.net",
	      "title": "n/a"
	    },
	    {
	      "url": "http://utro.ua",
	      "title": "n/a"
	    }
	  ]
	}
```

and with increased http client timeouts more than 2-3secs:

	$ ./bin/cmd  --parse-title=true \
		--connect-timeout=2s --readwrite-timeout=2s
		--input="@vlad @irina @alex (smile) (beer) like the unbreakable news; http://ukr.net http://unian.net http://korrespondent.net http://utro.ua"


```json
	{
	  "mentions": [
	    "vlad",
	    "irina",
	    "alex"
	  ],
	  "emoticons": [
	    "smile",
	    "beer"
	  ],
	  "links": [
	    {
	      "url": "http://ukr.net",
	      "title": "UKR.NET: Всі новини України, останні новини дня в Україні та Світі"
	    },
	    {
	      "url": "http://unian.net",
	      "title": "Новости УНИАН. Последние новости Украины и мира"
	    },
	    {
	      "url": "http://korrespondent.net",
	      "title": "Новости - последние новости Украины и мира сегодня  -  Korrespondent.net"
	    },
	    {
	      "url": "http://utro.ua",
	      "title": "Utro.UA - Новости Украины. Последние новости и события в Украине"
	    }
	  ]
	}
```

#### Final step

	$ ./bin/cmd --parse-title=true \
		--connect-timeout=2s --readwrite-timeout=2s \
	    --input="@bob @john (success) @datum (looser) visit with me (im) today a lot of sites; \
	    	https://twitter.com/ http://golang.org/ http://yandex.ua/ http://google.com/ http://payback.de/"

Result:

	- 2016/10/01 18:19:57 parser starting...
	- 2016/10/01 18:19:57 parser processing [@bob @john (success) @datum (looser) visit with me (im) today a lot of sites ; https://twitter.com/ http://golang.org/ http://yandex.ua/ http://google.com/ http://payback.de/]...
	- 2016/10/01 18:19:57 parser processing link[0]: https://twitter.com/
	- 2016/10/01 18:19:57 parser processing link[1]: http://golang.org/
	- 2016/10/01 18:19:57 parser processing link[2]: http://yandex.ua/
	- 2016/10/01 18:19:57 parser processing link[3]: http://google.com/
	- 2016/10/01 18:19:57 parser processing link[4]: http://payback.de/

```json
	{
	  "mentions": [
	    "bob",
	    "john",
	    "datum"
	  ],
	  "emoticons": [
	    "success",
	    "looser",
	    "im"
	  ],
	  "links": [
	    {
	      "url": "https://twitter.com/",
	      "title": "Twitter. It's what's happening."
	    },
	    {
	      "url": "http://golang.org/",
	      "title": "The Go Programming Language"
	    },
	    {
	      "url": "http://yandex.ua/",
	      "title": "Яндекс"
	    },
	    {
	      "url": "http://google.com/",
	      "title": "Google"
	    },
	    {
	      "url": "http://payback.de/",
	      "title": "Coupons, Aktionen \u0026 Gutscheine im PAYBACK Bonusprogramm!"
	    }
	  ]
	}
```

	- 2016/10/01 18:20:00 parser terminated [done]

Enjoy :) --> (smile)
